#!/bin/bash

# HMAC generated from payload + GH secret

curl -i \
  -H 'X-GitHub-Event: push' \
  -H 'X-Hub-Signature: sha1=<...>' \
  -H 'Content-Type: application/json' \
  -d '{"ref":"refs/heads/main","head_commit":{"id":"d9c7ccb0713da882d8373169e2a1d705f5959386"}}' \
  https://100.102.233.93:8443/api/v1/namespaces/tekton-pipelines/services/https:el-sample-github-listener:8080/proxy


# Bitbucket
curl -i -H 'X-GitHub-Event: push' -H 'X-Hub-Signature: sha1=1234' -H 'X-Event-Key: repo:refs_changed' -H 'Content-Type: application/json' -d '{"ref":"refs/heads/main","head_commit":{"id":"d9c7ccb0713da882d8373169e2a1d705f5959386"}}' http://100.102.233.93:31469/
